# Transformation

## Objective
- Transform E.coli cells with plasmid

## Key results
- Transfer 1ul of plasmid DNA from a PCR plate to 7.5ul of competent cells in a PCR plate
