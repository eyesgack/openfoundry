#OpenFoundry Equipment and Supplies

This is a list of equipment and supplies we recommend for setting up an OpenFoundry. 

#Equipment
1. OpenTrons OT2 (https://opentrons.com/ot-2)
    -This is the core liquid handling robot on which most OpenFoundry protocols are built.
    -Get the 8-channel p300 and p10 pipettes
    -If you can afford it, the MagDeck is also very useful for DNA purification and cleanup steps.
    -Price: $5500 for the OT2, $1500 for the MagDeck, $750 for shipping. Total: $7750
    , including both pipettes, MagDeck, HeatDeck, shipping and Illinois taxes.
2. Oxford Nanopore MinION (https://store.nanoporetech.com/starter-packs/)
    -This smartphone-sized DNA sequencer will enable you to sequence-verify all the constructs you build.
    -The starter pack is $1000-$1100 depending on shipping and comes with 2 flow cells and 1 sequencing kit, good for 6 sample preps
    -Flow cells are expensive ($900 each), but cheaper Flongle cells ($100) will become available soon.
    -Sequencing prep kits are also expensive ($400-$700). 
        -Need to figure out ways to make these kits last for more than 6 sample preps each
    