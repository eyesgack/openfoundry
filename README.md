# OpenFoundry protocols

Welcome to OpenFoundry protocols. This repo will grow in complexity over time, so stay tuned!

# Rules for OT2

1. 9,10,11 should have tipracks
2. 6,7,8 should not have talls (deep wells, tipracks, etc)
3. 1,2,3 should not have single addresses
4. Left mount uses 10ul multichannel
5. Right mount uses 300ul multichannel

